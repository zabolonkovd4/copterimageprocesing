#include "OpenCVHelper.h"

OpenCVHelper::OpenCVHelper()
{
    initialize();
}

OpenCVHelper::OpenCVHelper(const OpenCVHelper& orig) {
}

OpenCVHelper::~OpenCVHelper() {
}

void OpenCVHelper::initialize()
{
    _capture = shared_ptr<VideoCapture>(new VideoCapture(1));
    _capture->open(1);
    if (!_capture->isOpened())
    {
        _capture->open(0);
        if (!_capture->isOpened()) {};

	//status = ErrorCode::CameraNotFound;
    }
    else
    {
        //_capture->set(CV_CAP_PROP_FRAME_WIDTH, 1280);
        //_capture->set(CV_CAP_PROP_FRAME_HEIGHT, 820);
    }
    
   // status = ErrorCode::Success;
}

Mat OpenCVHelper::getFrame()
{
    Mat currentFrame;
    *_capture >> currentFrame;
    Mat frame = currentFrame.clone(); 

    //Check data in current frame
    if (!frame.data)
    { 
       /* status = ErrorCode::MarkerNotFound;*/
    }
    
    //status = ErrorCode::Success;
    
    return frame;
}

Mat OpenCVHelper::compute(Color color)
{
    Mat frame = getFrame();
    
    // Convert BGR to HSV
    Mat hsvFrame;
    cvtColor(frame, hsvFrame, COLOR_BGR2HSV);
    
    // Mat hsvFrameClone = hsvFrame.clone();
    Mat treshold2;
    
    // Threshold the HSV image
    inRange(hsvFrame, color.filterLow1, color.filterHigh1, treshold1);
    inRange(hsvFrame, color.filterLow2, color.filterHigh2, treshold2);
    treshold1 = treshold1 | treshold2;
    
    int erosion_size{1};
    Mat elementErosion = getStructuringElement(MORPH_ELLIPSE, 
        Size(2 * erosion_size + 1, 2 * erosion_size + 1), 
            Point(erosion_size, erosion_size));
    // Erode
    erode(treshold1, elementErosion, 1);
    
    int dilation_size{5};
    Mat elementDilate = getStructuringElement(MORPH_ELLIPSE, 
        Size(2 * dilation_size + 1, 2 * dilation_size + 1), 
            Point(dilation_size, dilation_size));
    // dilate
    dilate(treshold1, elementDilate, 1);
    
    // blog detector
    // Setup SimpleBlobDetector parameters.
    SimpleBlobDetector::Params blob_params;
    blob_params.minDistBetweenBlobs = 50;
    blob_params.filterByInertia = false;
    blob_params.filterByConvexity = false;
    blob_params.filterByColor = true;
    blob_params.blobColor = 255;
    blob_params.filterByCircularity = false;
    blob_params.filterByArea = false;
    // blob_params.minArea = 20;
    // blob_params.maxArea = 500;
    // Set up detector with params
    std::vector<KeyPoint> keypoints;
#if CV_MAJOR_VERSION < 3   
    SimpleBlobDetector simpleBlobDetector(blob_params);
    simpleBlobDetector.detect(treshold1, keypoints);
#else
    shared_ptr<SimpleBlobDetector> simpleBlobDetector = SimpleBlobDetector::create(blob_params);
    simpleBlobDetector->detect(treshold1, keypoints);
#endif
   
    // find largest blob
    if (keypoints.size() > 0)
    {
        auto kp_max = keypoints[0];
        for (auto& kp: keypoints)
        {
            if (kp.size > kp_max.size)
            {
                kp_max = kp;
            }
        }    
        // draw circle around the largest blob    
        circle(frame, kp_max.pt, int(kp_max.size), Scalar(0, 255, 0), 5);
    }
    
   
    return frame;
}

