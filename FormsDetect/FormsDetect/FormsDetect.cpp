﻿#include <iostream>

#include "OpenCVHelper.h"
using namespace std;


int main()
{
	/*int min = 100, max = 1000;
	int hmin =110, smin =100, vmin = 0,
		hmax = 150, smax = 255, vmax = 255;
	Mat frame, HSV, threshold, blurred, gray;
	VideoCapture capture;
	capture.open(0);
	if (!capture.isOpened()) {
		cout << "Camera is not open\n";
		exit(1);
	}
	else cout << "Camera is open\n";
	capture >> frame;
	namedWindow("original", WINDOW_AUTOSIZE);
	namedWindow("threshold", WINDOW_AUTOSIZE);
	while (1)
	{
		capture >> frame;


		cvtColor(frame, HSV, COLOR_BGR2HSV);
		medianBlur(HSV, blurred,17);
		cvtColor(blurred,blurred, COLOR_BGR2GRAY);
		inRange(blurred, Scalar(hmin, smin, vmin), Scalar(hmax, smax, vmax), threshold);

		threshold = 0.2*blurred + threshold;
		vector<Vec3f> circles;
		HoughCircles(threshold, circles, HOUGH_GRADIENT, 1,
			threshold.rows / 16,  
			100, 30,
			1, 100);
		for (size_t i = 0; i < circles.size(); i++)
		{
			Vec3i c = circles[i];
			Point center = Point(c[0], c[1]);
			circle(frame, center, 1, Scalar(0, 100, 100), 3, LINE_AA);
			int radius = c[2];
			circle(frame, center, radius, Scalar(255, 0, 255), 3, LINE_AA);
		}
		for (int y = 0; y < threshold.rows; y++) {
			for (int x = 0; x < threshold.cols; x++) {
				int value = threshold.at<uchar>(y, x);
				if (value == 255) {
					Rect rect;
					int count = floodFill(threshold, Point(x, y), Scalar(200), &rect);
					if (rect.width >= min && rect.width <= max
						&& rect.height >= min && rect.height <= max)
					{
						rectangle(frame, rect, Scalar(255, 0, 255, 4));

					}
				}
			}
		}

		imshow("original", frame);
		imshow("threshold", threshold);
		if (waitKey(33) == 27) break;	
	}*/
	namedWindow("original", WINDOW_AUTOSIZE);
	namedWindow("threshold", WINDOW_AUTOSIZE);
	Color color;
	color.filterLow1 = Scalar{ 0, 70, 50 };
	color.filterHigh1 = Scalar{ /*1*/0, 255, 255 };
	color.filterLow2 = Scalar{ /*170*/110, 70, 50 };
	color.filterHigh2 = Scalar{/* 180*/150, 255, 255 };
	printf("Program runned...\n");
	OpenCVHelper cvHelper;
	Mat frame = cvHelper.compute(color);
//	int i = 1;
	while (1)
	{
		Mat frame = cvHelper.compute(color);
		imshow("original", frame);
		imshow("threshold", cvHelper.treshold1);
		if (waitKey(33) == 27) break;
		/*imwrite("img" + std::to_string(i) + ".bmp", frame);
		imwrite("treshold1" + std::to_string(i) + ".bmp", cvHelper.treshold1);
		waitKey(100);
		++i;*/
	}







	return 0;
}
